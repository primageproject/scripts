#$option = Read-Host -Prompt 'Please, choose one'
#Set-Location $option

$CONTAINER_DIR_TEMP = '/tmp/.onedata_container_volume'
$CONTAINER_NAME = 'mounting_onedata'
$CONTAINER_IMAGE= 'primage/oneclient:ub1804_one1902'
$DEFAULT_MOUNT_POINT_NETWORK_DRIVE = 'z'

$_docker_inspect_running_cmd = "docker inspect " + $CONTAINER_NAME + " -f '{{ .State.Running }}'"
$_docker_inspect_ip_cmd = "docker exec -it " + $CONTAINER_NAME + " /sbin/ifconfig hvint0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}' "
$_docker_rm_cmd = "docker rm -f " + $CONTAINER_NAME 

function Get-Selection {
    $Title = "Please make a selection"
    $Message = "Select command to execute:"
    $Option1 = New-Object System.Management.Automation.Host.ChoiceDescription "mount"
    $Option2 = New-Object System.Management.Automation.Host.ChoiceDescription "clean"
    $Option3 = New-Object System.Management.Automation.Host.ChoiceDescription "Quit"
    $Options = [System.Management.Automation.Host.ChoiceDescription[]]($Option1,$Option2, $Option3)

    $host.ui.PromptForChoice($title, $message, $options, 0)
}

function Mount-Onedata {
    param (
        $_mount_point,
        $_container_ip
    )
    $_cmd = 'net use ' +$_mount_point +': \\'+ $_container_ip + '\onedata\onedata /user:guest "nopassword"'

    try {
        #Write-Host $_cmd
        Invoke-Expression $_cmd
        Write-Host 'Onedata mounted successfully at '$_mount_point':'
    }catch{
        Write-Error 'Something wrong mounting Onedata, stopping Docker container'
        Invoke-Expression $_docker_rm_cmd
        Write-Host "Docker container stopped successfully"
    }

}

function Unmount-Onedata {
    param (
        $_mount_point
    )
    $_cmd = 'net use ' + $_mount_point + ': /delete'
    Invoke-Expression $_cmd
}

function Ask-user {
    param (
        $message
    )
    $resp = "N"
    $read_data = ""
    while ( ($resp -ne "Y") -and ($resp -ne "Yes") -and ($resp -ne "yes") -and ($resp -ne "y") ) {
        $read_data = Read-Host -Prompt $message
        Write-Host 'You inserted: '$read_data
        $resp = Read-Host -Prompt 'Is it correct? (Y / N)'
    }
    return $read_data
}

function Get-DockerIP {
    $ip = ""
    $cmd = "docker exec -it " + $CONTAINER_NAME + " /sbin/ifconfig hvint0 " #| grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}' "
    
    $inet_addr =  Invoke-Expression $cmd | Select-String "inet"
    $inet_addr = [string] $inet_addr
    $ip = $inet_addr.Trim().Split()[1]

    return $ip
}
function Start-Docker {
    param (
        $ONEDATA_TOKEN,
        $ONECLIENT_PROVIDER_HOST,
        $MOUNT_POINT_NETWORK_DRIVE
    )

    $_docker_run_cmd = 'docker run -dit --privileged -v '+ $CONTAINER_DIR_TEMP +':/mnt/onedata -e MOUNT_POINT=/mnt/onedata -e ONECLIENT_ACCESS_TOKEN=' + $ONEDATA_TOKEN +' -e ONECLIENT_PROVIDER_HOST=' + $ONECLIENT_PROVIDER_HOST + ' --name ' + $CONTAINER_NAME + ' --network=host '+ $CONTAINER_IMAGE + ' bash'

    Write-Host  "Executing Docker container..."
    try {

        $contained_id = Invoke-Expression $_docker_run_cmd
    } catch {
        Write-Error -Message 'Cannot start the Docker container '
        Exit
    }

    $_container_is_running = "false"
    while ( $_container_is_running -ne "true" ){
        Start-Sleep -s 10
        Write-Host "Wait until the docker container is running..."
        $_container_is_running = Invoke-Expression $_docker_inspect_running_cmd
    }
    Write-Host "Docker container started successfully"
    $_container_ip = Get-DockerIP
    if (-not $_container_ip){
        Write-Error -Message "Cannot obtain the Docker container IP"
        Exit
    }
    return $_container_ip
}

function Show-Menu {
    Write-Host 'PRIMAGE - Accessing Onedata Windows Script using Docker'
    switch (Get-Selection) {
        0 {
            Write-Host 'You chose option "mount". Now, I need more information'
            $ONEDATA_TOKEN = Ask-user 'Please, insert the onedata token for registration. Example: MDAxY2...5bl8JAo'
            $ONECLIENT_PROVIDER_HOST = Ask-user 'Please, insert the Oneprovider. PRIMAGE oneproviders: plg-cyfronet-01.datahub.egi.eu or upv.datahub.egi.eu'
            $MOUNT_POINT_NETWORK_DRIVE = Ask-user 'Please, insert any available network drive letter. For example: z'

            if (-not $MOUNT_POINT_NETWORK_DRIVE ){ 
                Write-Host 'Using '$DEFAULT_MOUNT_POINT_NETWORK_DRIVE' as network drive mount point' 
                $MOUNT_POINT_NETWORK_DRIVE = $DEFAULT_MOUNT_POINT_NETWORK_DRIVE
            }  

            if ( (-not $ONEDATA_TOKEN ) -or (-not $ONECLIENT_PROVIDER_HOST) -or (-not $MOUNT_POINT_NETWORK_DRIVE) ) {
                Write-Error -Message 'Credential token, Oneprovider host or mount point are not defined'
                Exit
            }

            if (Get-PSDrive $MOUNT_POINT_NETWORK_DRIVE -ErrorAction SilentlyContinue) {
                Get-PSDrive $MOUNT_POINT_NETWORK_DRIVE -ErrorAction SilentlyContinue
                Write-Error -Message 'The "$MOUNT_POINT_NETWORK_DRIVE": drive is already in use.'
                Exit
            }

            $CONTAINER_IP = Start-Docker $ONEDATA_TOKEN $ONECLIENT_PROVIDER_HOST $MOUNT_POINT_NETWORK_DRIVE
            
            Mount-Onedata $MOUNT_POINT_NETWORK_DRIVE $CONTAINER_IP

        }
        1 {
            Write-Host 'You chose option "clean"'
            
            $_drive= Get-PSDrive | where {$_.DisplayRoot -like "*\onedata\onedata"} | Select Name
            if ($_drive.name)
            {
                $str = 'Detected 1 Onedata mounted drive (' + $_drive.name + ':). Do you want remove it? (Y / N)'
                $resp = Read-Host -Prompt $str
                if (($resp -eq "Y") -or ($resp -eq "Yes") -or ($resp -eq "yes") -or ($resp -eq "y")){
                    Unmount-Onedata $_drive.name
                }
            }
            $_container_is_running = "false"
            try {
                $_container_is_running = Invoke-Expression $_docker_inspect_running_cmd
            }catch{
                Write-Debug -Message "Container is not running"
            }
            if ($_container_is_running -eq "true"){
                $delete_info = Invoke-Expression $_docker_rm_cmd
                Write-Host "Docker container stopped successfully"
            }
        }
        2 {
            Write-Host 'Quit'
        }
    }
}

Show-Menu 