#!/bin/bash

CONTAINER_DIR_TEMP=/tmp/.onedata_container_volume
MOUNT_POINT_TEMP=/tmp/.onedata_mount_point
CONTAINER_NAME=mounting_onedata

help () {
    echo "$0 Usage:"
    echo " $0 [OS] [Command] [Command arguments]"
    echo "Operating systems (OS) availables: "
    echo -e "\t- linux: Linux OS (tested on Ubuntu and Linux Mint)"
    echo -e "\t- mac: MacOS tested on MacOS Sierra"
    echo "Commands availables: "
    echo -e "\t- clean: Unmount Onedata (if mounted) and remove the docker container"
    echo -e "\t- mount AUTH_TOKEN ONEPROVIDER_HOST MOUNT_POINT_DIRECTORY: Mounts Onedata at [MOUNT_POINT_DIRECTORY] by connecting with [ONEPROVIDER_HOST] using the authentication token [AUTH_TOKEN]"
}

clean () {
    umount $MOUNT_POINT_TEMP > /dev/null 2> /dev/null
    docker rm -f $CONTAINER_NAME > /dev/null 2> /dev/null
    rm -rf $CONTAINER_DIR_TEMP
    rm -rf $MOUNT_POINT_TEMP
}

mount_onedata () {
    OS=$1
    ONEDATA_TOKEN=$3
    ONECLIENT_PROVIDER_HOST=$4
    ONEDATA_DIRECTORY_MOUNT=$5 

    if [ $OS == "linux" ]; then
        MOUNT_POINT_TEMP=$ONEDATA_DIRECTORY_MOUNT
    fi

    clean $MOUNT_POINT_TEMP

    mkdir -p $CONTAINER_DIR_TEMP
    mkdir -p $MOUNT_POINT_TEMP

    echo "Executing Docker container..."
    _container_id=$(docker run -dit --privileged -v $CONTAINER_DIR_TEMP:/mnt/onedata -e MOUNT_POINT=/mnt/onedata -e ONECLIENT_ACCESS_TOKEN=$ONEDATA_TOKEN -e ONECLIENT_PROVIDER_HOST=$ONECLIENT_PROVIDER_HOST --name $CONTAINER_NAME -p 445:445 -p 169:169 primage/oneclient:ub1804_one1902 bash)

    RESULT=$?
    if [ $RESULT -ne 0 ]; then
        exit 1
    fi 

    while [ "$(docker inspect $CONTAINER_NAME -f '{{ .State.Running }}')" != "true" ] 
    do
        echo "Wait until the docker container is running..."
        sleep 5
    done
    echo "Docker container started successfully"

    if [ $OS == "mac" ]; then
        mount_smbfs //guest@localhost/onedata/onedata $MOUNT_POINT_TEMP 
    else
        CONTAINER_IP=$(docker inspect $CONTAINER_NAME -f '{{ .NetworkSettings.IPAddress }}')
        sleep 5
        mount -t cifs -o guest //$CONTAINER_IP/onedata/onedata $MOUNT_POINT_TEMP  
    fi
    RESULT=$?

    if [ $RESULT -eq 0 ]; then
        if [ $OS == "mac" ]; then
            rm -rf $ONEDATA_DIRECTORY_MOUNT > /dev/null 2> /dev/null
            ln -sf $MOUNT_POINT_TEMP $ONEDATA_DIRECTORY_MOUNT
        fi
        echo "Onedata mounted successfully at: $ONEDATA_DIRECTORY_MOUNT"
    else
        echo "Error mounting Onedata, removing Docker container..."
        docker rm -f $CONTAINER_NAME > /dev/null 2> /dev/null
        exit 1
    fi
}

if (( $# < 2 )); then
    help
    exit 1
fi

case $2 in 
    clean)
		if (( $# == 3 )); then
			MOUNT_POINT_TEMP=$3
		fi
		clean
		
        exit 0
        ;;
    mount)
        if (( $# != 5 )); then
            echo "Error: more arguments are required"
            help
            exit 1
        fi
        mount_onedata $@
    ;;
    *)
        echo "Error: invalid argument"
        help
        exit 1

esac

